Oficina de testes
=================

### 16/09/2019

Começemos por definir uma classe de conta corrente

    class ContaCorrente:
        saldo = 0

E então pode começar a escrever nossos testes.

    import unittest

    class TesteContaCorrente(unittest.TestCase):
        def teste_inicio(self):
            cc = ContaCorrente()
            self.assertAlmostEqual(cc.saldo, 0)

    suite = unittest.TestLoader().loadTestsFromTestCase(TesteContaCorrente)
    unittest.TextTestRunner(verbosity=2).run(suite)

    ## <unittest.runner.TextTestResult run=1 errors=0 failures=0>
    ## 
    ## teste_inicio (__main__.TesteContaCorrente) ... ok
    ## 
    ## ----------------------------------------------------------------------
    ## Ran 1 test in 0.000s
    ## 
    ## OK

Agora precisamos evoluir nossa classe para poder movimentar valores em
nossa conta corrente.

    class ContaCorrente:
        saldo = 0
        def deposito(self, valor):
            self.saldo += valor
        
        def saque(self, valor):
            self.saldo -= valor
        
        def __str__(self):
            return str(self.saldo)

E agora o teste

    import unittest

    class TesteContaCorrente(unittest.TestCase):
        def teste_inicio(self):
            cc = ContaCorrente()
            self.assertEqual(cc.saldo, 0)
          
        def teste_deposito(self):
            cc = ContaCorrente()
            cc.deposito(10)
            self.assertEqual(cc.saldo, 10)
            
        def teste_saque(self):
            cc = ContaCorrente()
            cc.deposito(10)
            cc.saque(10)
            self.assertEqual(cc.saldo, 0)


    suite = unittest.TestLoader().loadTestsFromTestCase(TesteContaCorrente)
    unittest.TextTestRunner(verbosity=2).run(suite)

    ## <unittest.runner.TextTestResult run=3 errors=0 failures=0>
    ## 
    ## teste_deposito (__main__.TesteContaCorrente) ... ok
    ## teste_inicio (__main__.TesteContaCorrente) ... ok
    ## teste_saque (__main__.TesteContaCorrente) ... ok
    ## 
    ## ----------------------------------------------------------------------
    ## Ran 3 tests in 0.000s
    ## 
    ## OK

Vamos tornar nossa classe mais complexa. Não podemos fazer saque caso
nossa conta não tenha saldo.

    class ContaCorrente:
        saldo = 0
        
        def deposito(self, valor):
            self.saldo += valor
        
        def saque(self, valor):
            if self.saldo >= valor: 
                self.saldo -= valor
        
        def __str__(self):
            return str(self.saldo)

E agora adicionamos testes para verificar o comportamento.

    import unittest

    class TesteContaCorrente(unittest.TestCase):
        def teste_inicio(self):
            cc = ContaCorrente()
            self.assertEqual(cc.saldo, 0)
          
        def teste_deposito(self):
            cc = ContaCorrente()
            cc.deposito(10)
            self.assertEqual(cc.saldo, 10)
            
        def teste_saque(self):
            cc = ContaCorrente()
            cc.deposito(10)
            cc.saque(10)
            self.assertEqual(cc.saldo, 0)
            
            # Tentativa de saque quando a conta está zerada
            cc.saque(10)
            self.assertEqual(cc.saldo, 0)
            
            # Tentar sacar quantia maior que saldo
            cc.deposito(5)
            cc.saque(10)
            assert cc.saldo == 5



    suite = unittest.TestLoader().loadTestsFromTestCase(TesteContaCorrente)
    unittest.TextTestRunner(verbosity=2).run(suite)

    ## <unittest.runner.TextTestResult run=3 errors=0 failures=0>
    ## 
    ## teste_deposito (__main__.TesteContaCorrente) ... ok
    ## teste_inicio (__main__.TesteContaCorrente) ... ok
    ## teste_saque (__main__.TesteContaCorrente) ... ok
    ## 
    ## ----------------------------------------------------------------------
    ## Ran 3 tests in 0.001s
    ## 
    ## OK

Vamos incluir um método de transferência

    class ContaCorrente:
        saldo = 0
        
        def deposito(self, valor):
            self.saldo += valor
        
        def saque(self, valor):
            if self.saldo >= valor: 
                self.saldo -= valor
                return True
            return False
        
        def transferencia(self, conta_destino, valor):
            if self.saque(valor):
                conta_destino.deposito(valor)
        
        def __str__(self):
            return str(self.saldo)

E os testes:

    import unittest

    class TesteContaCorrente(unittest.TestCase):
        def teste_inicio(self):
            cc = ContaCorrente()
            self.assertEqual(cc.saldo, 0)
          
        def teste_deposito(self):
            cc = ContaCorrente()
            cc.deposito(10)
            self.assertEqual(cc.saldo, 10)
            
        def teste_saque(self):
            cc = ContaCorrente()
            cc.deposito(10)
            cc.saque(10)
            self.assertEqual(cc.saldo, 0)
            
            # Tentativa de saque quando a conta está zerada
            cc.saque(10)
            self.assertEqual(cc.saldo, 0)
            
            # Tentar sacar quantia maior que saldo
            cc.deposito(5)
            cc.saque(10)
            self.assertEqual(cc.saldo, 5)
            
        def teste_transferencia(self):
            cc1 = ContaCorrente()
            cc2 = ContaCorrente()
            
            cc1.transferencia(cc2, 1)
            self.assertEqual(cc1.saldo, 0)
            self.assertEqual(cc2.saldo, 0)
            
            cc1 = ContaCorrente()
            cc2 = ContaCorrente()
            cc1.deposito(10)
            cc1.transferencia(cc2, 1)
            self.assertEqual(cc1.saldo, 9)
            self.assertEqual(cc2.saldo, 1)



    suite = unittest.TestLoader().loadTestsFromTestCase(TesteContaCorrente)
    unittest.TextTestRunner(verbosity=2).run(suite)

    ## <unittest.runner.TextTestResult run=4 errors=0 failures=0>
    ## 
    ## teste_deposito (__main__.TesteContaCorrente) ... ok
    ## teste_inicio (__main__.TesteContaCorrente) ... ok
    ## teste_saque (__main__.TesteContaCorrente) ... ok
    ## teste_transferencia (__main__.TesteContaCorrente) ... ok
    ## 
    ## ----------------------------------------------------------------------
    ## Ran 4 tests in 0.000s
    ## 
    ## OK

E se tivéssemos cheque especial em nossa conta? Como nossas regras
deveriam se comportar?

    class ContaCorrente:
        saldo = 0
        especial = 100
        
        def deposito(self, valor):
            self.saldo += valor
        
        def saque(self, valor):
            if self.saldo + self.especial >= valor: 
                self.saldo -= valor
                return True
            return False
        
        def limite(self, valor):
            self.especial = valor
            
        def transferencia(self, conta_destino, valor):
            if self.saque(valor):
                conta_destino.deposito(valor)
        
        def __str__(self):
            return str(self.saldo)
            
    suite = unittest.TestLoader().loadTestsFromTestCase(TesteContaCorrente)
    unittest.TextTestRunner(verbosity=2).run(suite)

    ## <unittest.runner.TextTestResult run=4 errors=0 failures=2>
    ## 
    ## teste_deposito (__main__.TesteContaCorrente) ... ok
    ## teste_inicio (__main__.TesteContaCorrente) ... ok
    ## teste_saque (__main__.TesteContaCorrente) ... FAIL
    ## teste_transferencia (__main__.TesteContaCorrente) ... FAIL
    ## 
    ## ======================================================================
    ## FAIL: teste_saque (__main__.TesteContaCorrente)
    ## ----------------------------------------------------------------------
    ## Traceback (most recent call last):
    ##   File "<string>", line 19, in teste_saque
    ## AssertionError: -10 != 0
    ## 
    ## ======================================================================
    ## FAIL: teste_transferencia (__main__.TesteContaCorrente)
    ## ----------------------------------------------------------------------
    ## Traceback (most recent call last):
    ##   File "<string>", line 31, in teste_transferencia
    ## AssertionError: -1 != 0
    ## 
    ## ----------------------------------------------------------------------
    ## Ran 4 tests in 0.000s
    ## 
    ## FAILED (failures=2)

Ops! A nova regra de negócio alterou os comportamentos esperados
anteriormente.

Vamos mudar nossas expectativas.

    import unittest

    class TesteContaCorrente(unittest.TestCase):
        def teste_inicio(self):
            cc = ContaCorrente()
            self.assertEqual(cc.saldo, 0)
          
        def teste_deposito(self):
            cc = ContaCorrente()
            cc.deposito(10)
            self.assertEqual(cc.saldo, 10)
            
        def teste_saque(self):
            cc = ContaCorrente()
            cc.deposito(10)
            cc.saque(10)
            self.assertEqual(cc.saldo, 0)
            
            # Tentativa de saque quando a conta está zerada
            # permitida pela regra que inclui limite inicial
            cc.saque(10)
            self.assertEqual(cc.saldo, -10) #alterado
            
            # Tentar sacar quantia maior que saldo + limite
            cc.deposito(5)
            cc.saque(100) # alterado
            self.assertEqual(cc.saldo, -5) # alterado
        
        def teste_transferencia(self):
            cc1 = ContaCorrente()
            cc2 = ContaCorrente()
            
            cc1.transferencia(cc2, 1)
            self.assertEqual(cc1.saldo, -1) # alterado
            self.assertEqual(cc2.saldo, 1) # alterado
            
            cc1 = ContaCorrente()
            cc2 = ContaCorrente()
            cc1.deposito(10)
            cc1.transferencia(cc2, 1)
            self.assertEqual(cc1.saldo, 9)
            self.assertEqual(cc2.saldo, 1)
            
            # Transferencia de valor maior que saldo + limite
            cc1 = ContaCorrente()
            cc2 = ContaCorrente()
            cc1.transferencia(cc2, 1000)
            self.assertEqual(cc1.saldo, 0)
            self.assertEqual(cc2.saldo, 0)

    suite = unittest.TestLoader().loadTestsFromTestCase(TesteContaCorrente)
    unittest.TextTestRunner(verbosity=2).run(suite)

    ## <unittest.runner.TextTestResult run=4 errors=0 failures=0>
    ## 
    ## teste_deposito (__main__.TesteContaCorrente) ... ok
    ## teste_inicio (__main__.TesteContaCorrente) ... ok
    ## teste_saque (__main__.TesteContaCorrente) ... ok
    ## teste_transferencia (__main__.TesteContaCorrente) ... ok
    ## 
    ## ----------------------------------------------------------------------
    ## Ran 4 tests in 0.000s
    ## 
    ## OK
