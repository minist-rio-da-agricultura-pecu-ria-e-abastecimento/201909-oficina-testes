# 092019-oficina-testes

Projeto para oficina de testes que ocorrerá dia 16/09/2019.

# Linhas gerais

A idea é ter o conteúdo da oficina dentro de `oficina.md` para que possa
acompanhado por todos os participantes e funcionar posteriormente como
documentação.

## Fluxo de trabalho

As modificações devem ocorrer nos arquivos `contacorrente.py` e `teste_contacorrente.py`.

Após realizar as alteraçãoes nos arquivos, o comando abaixo deve ser executado 
para que os testes rodem.

```
python -m unittest -v
```
